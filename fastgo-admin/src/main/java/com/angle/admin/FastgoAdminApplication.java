package com.angle.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class FastgoAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(FastgoAdminApplication.class, args);
    }
}
