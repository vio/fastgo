package com.angle.fastgo.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastgoCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastgoCommonApplication.class, args);
    }

}
