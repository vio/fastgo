package com.angle.fastgo.common.enums;

import lombok.Getter;

/**
 * description: 系统异常枚举类
 * 错误码和错误信息定义类
 * 1. 错误码定义规则为5为数字
 * 2. 前两位表示业务场景，最后三位表示错误码。例如：100001。10:通用 001:系统未知异常
 * 3. 维护错误码后需要维护错误描述，将他们定义为枚举形式
 * 错误码列表：
 *  10: 通用
 *      001：参数格式校验
 *  11: 商品
 *  12: 订单
 *  13: 购物车
 *  14: 物流
 * version: 1.0
 * date: 2020/11/21 16:52
 * author: zhuwei
 */
@Getter
public enum BizCodeEnums {

    /**
     * 系统通用级别的异常枚举
     */
    SYSTEM_UNKNOW_ERROR(10000,"系统未知错误"),

    PARAM_VAILD_ERROR(10001,"参数校验错误"),

    PARAM_NOT_EMPTY(10002,"参数不能为空"),
    ;

    /**
     * 异常码
     */
    private Integer code;

    /**
     * 异常信息
     */
    private String msg;


    BizCodeEnums(Integer code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
