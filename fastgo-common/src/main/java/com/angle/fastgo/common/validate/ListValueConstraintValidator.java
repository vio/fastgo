package com.angle.fastgo.common.validate;

import com.angle.fastgo.common.anno.ListValue;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * description: @ListValue注解校验器
 * version: 1.0
 * date: 2020/11/21 17:58
 * author: zhuwei
 */
public class ListValueConstraintValidator implements ConstraintValidator<ListValue,Integer> {

    private Set<Integer> listValues = new HashSet<>();

    /**
     * description: 校验器初始化方法,为了拿到listValue注解的信息
     * version: 1.0
     * date: 2020/11/21 17:59
     * author: zhuwei
     * @param constraintAnnotation 注解对象
     * @return void
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {
        int [] values = constraintAnnotation.value();
        for (int value: values) {
            listValues.add(value);
        }
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return listValues.contains(value);
    }
}
