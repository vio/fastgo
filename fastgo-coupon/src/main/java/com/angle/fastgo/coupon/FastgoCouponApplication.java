package com.angle.fastgo.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@MapperScan(basePackages = {"com.angle.fastgo.coupon.dao"})
@EnableDiscoveryClient
public class FastgoCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastgoCouponApplication.class, args);
    }

}
