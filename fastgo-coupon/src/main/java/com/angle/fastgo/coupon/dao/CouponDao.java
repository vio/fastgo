package com.angle.fastgo.coupon.dao;

import com.angle.fastgo.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:05:23
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
