package com.angle.fastgo.coupon.dao;

import com.angle.fastgo.coupon.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:05:22
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
