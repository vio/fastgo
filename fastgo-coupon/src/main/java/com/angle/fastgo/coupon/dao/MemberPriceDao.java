package com.angle.fastgo.coupon.dao;

import com.angle.fastgo.coupon.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品会员价格
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:05:22
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {
	
}
