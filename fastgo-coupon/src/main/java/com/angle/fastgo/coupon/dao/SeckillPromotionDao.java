package com.angle.fastgo.coupon.dao;

import com.angle.fastgo.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:05:22
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
