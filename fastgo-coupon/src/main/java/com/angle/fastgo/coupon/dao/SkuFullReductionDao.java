package com.angle.fastgo.coupon.dao;

import com.angle.fastgo.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:05:21
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
