package com.angle.fastgo.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.coupon.entity.CouponSpuRelationEntity;

import java.util.Map;

/**
 * 优惠券与产品关联
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:05:22
 */
public interface CouponSpuRelationService extends IService<CouponSpuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

