package com.angle.fastgo.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * description: 网关跨域请求配置类
 * 跨域官网说明: https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Access_control_CORS
 * 1、添加响应头
 * • Access-Control-Allow-Origin：支持哪些来源的请求跨域
 * • Access-Control-Allow-Methods：支持哪些方法跨域
 * • Access-Control-Allow-Credentials：跨域请求默认不包含cookie，设置为true可以包含 cookie
 * • Access-Control-Expose-Headers：跨域请求暴露的字段
 * • CORS请求时，XMLHttpRequest对象的getResponseHeader()方法只能拿到6个基本字段：
 *     Cache-Control、Content-Language、Content-Type、Expires、Last-Modified、Pragma。
 *     如 果想拿到其他字段，就必须在Access-Control-Expose-Headers里面指定。
 * • Access-Control-Max-Age：表明该响应的有效时间为多少秒。在有效时间内，浏览器无 须为同一请求再次发起预检请求。
 *     请注意，浏览器自身维护了一个最大有效时间，如果 该首部字段的值超过了最大有效时间，将不会生效。
 * version: 1.0
 * date: 2020/11/7 17:58
 * author: zhuwei
 */
@Configuration
public class GateWayCorsConfig {

    /**
     * description: 跨域filter的配置
     * version: 1.0
     * date: 2020/11/7 17:59
     * author: zhuwei
     */
    @Bean
    public CorsWebFilter corsWebFilter() {

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration corsConfiguration = new CorsConfiguration();

        //1、配置跨域
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.setAllowCredentials(true);

        source.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsWebFilter(source);
    }
}
