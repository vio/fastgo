package com.angle.fastgo.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@MapperScan(basePackages = {"com.angle.fastgo.member.dao"})
@EnableFeignClients(basePackages = {"com.angle.fastgo.member.feignclient"})
@SpringBootApplication
public class FastgoMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastgoMemberApplication.class, args);
    }

}
