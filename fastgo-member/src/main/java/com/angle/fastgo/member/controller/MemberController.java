package com.angle.fastgo.member.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import com.angle.fastgo.member.feignclient.CouponFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.angle.fastgo.member.entity.MemberEntity;
import com.angle.fastgo.member.service.MemberService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.R;



/**
 * 会员
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:00:31
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Autowired
    private CouponFeignClient couponFeignClient;

    /**
     * 测试feign接口调用
     * @return
     */
    @RequestMapping("/memberAllCouponList")
    public R memberAllCouponList() {
        return couponFeignClient.list(new HashMap<>());
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
