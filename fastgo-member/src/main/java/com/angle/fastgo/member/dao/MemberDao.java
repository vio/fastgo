package com.angle.fastgo.member.dao;

import com.angle.fastgo.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:00:31
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
