package com.angle.fastgo.member.dao;

import com.angle.fastgo.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:00:31
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
