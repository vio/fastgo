package com.angle.fastgo.member.feignclient;

import com.angle.fastgo.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


/**
 * description: TODO
 * version: 1.0
 * date: 2020/10/25 20:16
 * author: zhuwei
 */ 
@FeignClient(name = "fastgo-coupon",path = "/coupon")
public interface CouponFeignClient {

    /**
     * description: 查询会员所有的优惠卷(用于测试feign接口)
     * version: 1.0
     * date: 2020/10/25 20:14
     * author: zhuwei
     * @param params 查询参数
     * @return
     */ 
    @RequestMapping("/coupon/list")
    R list(@RequestParam Map<String, Object> params);
}
