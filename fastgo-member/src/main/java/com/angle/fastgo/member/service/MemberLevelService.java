package com.angle.fastgo.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:00:31
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

