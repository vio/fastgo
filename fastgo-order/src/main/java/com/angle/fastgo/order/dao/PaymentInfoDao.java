package com.angle.fastgo.order.dao;

import com.angle.fastgo.order.entity.PaymentInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付信息表
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:40:24
 */
@Mapper
public interface PaymentInfoDao extends BaseMapper<PaymentInfoEntity> {
	
}
