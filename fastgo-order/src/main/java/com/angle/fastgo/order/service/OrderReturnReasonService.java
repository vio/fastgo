package com.angle.fastgo.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.order.entity.OrderReturnReasonEntity;

import java.util.Map;

/**
 * 退货原因
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:40:24
 */
public interface OrderReturnReasonService extends IService<OrderReturnReasonEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

