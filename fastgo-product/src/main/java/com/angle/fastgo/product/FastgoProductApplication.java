package com.angle.fastgo.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.angle.fastgo.product.dao"})
public class FastgoProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastgoProductApplication.class, args);
    }

}
