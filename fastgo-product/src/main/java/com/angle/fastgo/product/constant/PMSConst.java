package com.angle.fastgo.product.constant;

/**
 * description: 商品模块所有涉及的常量
 * version: 1.0
 * date: 2020/11/7 15:20
 * author: zhuwei
 */
public class PMSConst {

    /**
     * 一级分类了
     */
    public static final Long CATEGORY_TOP_LEVEL = 0l;
}
