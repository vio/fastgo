package com.angle.fastgo.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.angle.fastgo.common.enums.BizCodeEnums;
import com.angle.fastgo.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.angle.fastgo.product.entity.AttrGroupEntity;
import com.angle.fastgo.product.service.AttrGroupService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.R;



/**
 * 属性分组
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {

    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    /**
     * description: 根据三级分类ID查询 商品的分组信息
     * version: 1.0
     * date: 2020/11/21 21:22
     * author: zhuwei
     * @param params 查询条件
        {
        page: 1,//当前页码
        limit: 10,//每页记录数
        sidx: 'id',//排序字段
        order: 'asc/desc',//排序方式
        key: '华为'//检索关键字
        }
     * @param catelogId 三级分类ID
     * @return R
     */
    @RequestMapping("/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params,@PathVariable("catelogId") Long catelogId ){
        /**
         * 校验参数不能为空
         */
        if(null == catelogId) {
            return R.error(BizCodeEnums.PARAM_NOT_EMPTY.getCode(),BizCodeEnums.PARAM_NOT_EMPTY.getMsg()+"catelogId");
        }
        PageUtils page = attrGroupService.queryPageByCategoryId(params,catelogId);

        return R.ok().put("page", page);
    }


    /**
     * description: 或者商品属性分组详情信息
     * version: 1.0
     * date: 2020/11/22 17:07
     * author: zhuwei
     * @param attrGroupId 商品属性详情分组
     * @return R
     */
    @RequestMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        List<Long> categoryPath = categoryService.qryCategoryPathById(attrGroup.getCatelogId());
        attrGroup.setCatelogPath(categoryPath);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
