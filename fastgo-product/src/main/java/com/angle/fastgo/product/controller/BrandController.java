package com.angle.fastgo.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.angle.fastgo.common.validate.AddMaker;
import com.angle.fastgo.common.validate.ModifyMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.angle.fastgo.product.entity.BrandEntity;
import com.angle.fastgo.product.service.BrandService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.R;

import javax.validation.Valid;


/**
 * 品牌
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * description: 保存品牌接口
     * version: 1.0
     * date: 2020/11/21 16:03
     * author: zhuwei
     * @param brand 品牌实体对象
     * @return R
     */
    @RequestMapping("/save")
    public R save(@Validated(value = {AddMaker.class}) @RequestBody BrandEntity brand ){
        brandService.save(brand);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@Validated(value = {ModifyMaker.class}) @RequestBody BrandEntity brand){
		brandService.updateByIdWithRelDate(brand);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
