package com.angle.fastgo.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import com.angle.fastgo.common.enums.BizCodeEnums;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.angle.fastgo.product.entity.CategoryBrandRelationEntity;
import com.angle.fastgo.product.service.CategoryBrandRelationService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.R;



/**
 * 品牌分类关联
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
@RestController
@RequestMapping("product/categorybrandrelation")
public class CategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = categoryBrandRelationService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * description: 查询品牌关联分类表
     * 比如品牌华为管理的分类有 手机 电视 电脑 等
     * version: 1.0
     * date: 2020/11/22 18:28
     * author: zhuwei
     * @param brandId 品牌ID
     * @return R返回结果
     */
    @RequestMapping("/catelog/list")
    public R list(@RequestParam ("brandId") Long brandId ){
        if(null == brandId) {
            return R.error(BizCodeEnums.PARAM_NOT_EMPTY);
        }
        List<CategoryBrandRelationEntity> brandRelationEntityList =  categoryBrandRelationService.list(
                new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id",brandId)
        );

        return R.ok().put("data", brandRelationEntityList);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);

        return R.ok().put("categoryBrandRelation", categoryBrandRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.saveDetail(categoryBrandRelation);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.updateById(categoryBrandRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
