package com.angle.fastgo.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.angle.fastgo.product.entity.CategoryEntity;
import com.angle.fastgo.product.service.CategoryService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.R;



/**
 * 商品三级分类
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * description: 查询商品的分类以及子分类,并且用树形结构展示
     * version: 1.0
     * date: 2020/11/7 14:32
     * author: zhuwei
     * @return
     */
    @RequestMapping("/list/tree")
    public R list(){
        List<CategoryEntity> categoryEntityList = categoryService.listTree();
        return R.ok().put("data", categoryEntityList);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{catId}")
    public R info(@PathVariable("catId") Long catId){
		CategoryEntity category = categoryService.getById(catId);

        return R.ok().put("data", category);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryEntity category){
		categoryService.save(category);

        return R.ok();
    }

    /**
     * description: 树菜单拖拽更新
     * version: 1.0
     * date: 2020/11/17 22:44
     * author: zhuwei
     * @param categoryEntities 拖拽待更新的节点
     * @return R
     */
    @RequestMapping("/update/sort")
    public R updateDropAction(@RequestBody List<CategoryEntity> categoryEntities) {
        categoryService.updateBatchById(categoryEntities);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryEntity category){
		categoryService.updateByIdWithRelDate(category);
        return R.ok();
    }

    /**
     * description: 批量删除商品分类
     * version: 1.0
     * date: 2020/11/8 16:15
     * @author: zhuwei
     * @param catIds 商品分类ID集合
     * @return R
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] catIds){
        categoryService.batchDeleteByIds(Arrays.asList(catIds));
        return R.ok();
    }

}
