package com.angle.fastgo.product.dao;

import com.angle.fastgo.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
