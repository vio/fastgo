package com.angle.fastgo.product.dao;

import com.angle.fastgo.product.entity.SpuCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:52
 */
@Mapper
public interface SpuCommentDao extends BaseMapper<SpuCommentEntity> {
	
}
