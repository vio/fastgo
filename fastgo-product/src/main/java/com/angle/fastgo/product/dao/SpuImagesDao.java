package com.angle.fastgo.product.dao;

import com.angle.fastgo.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:52
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
