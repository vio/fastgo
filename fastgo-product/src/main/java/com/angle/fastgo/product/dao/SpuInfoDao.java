package com.angle.fastgo.product.dao;

import com.angle.fastgo.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:52
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
