package com.angle.fastgo.product.entity;

import com.angle.fastgo.common.anno.ListValue;
import com.angle.fastgo.common.validate.AddMaker;
import com.angle.fastgo.common.validate.ModifyMaker;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import jdk.nashorn.internal.runtime.regexp.JdkRegExp;
import jdk.nashorn.internal.runtime.regexp.RegExp;
import jdk.nashorn.internal.runtime.regexp.RegExpMatcher;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 * 保存的时候,商品ID 自定生成不能带入
	 */
	@TableId
	@Null(message = "保存品牌不需要指定ID",groups = {AddMaker.class})
	@NotNull(message = "修改商品品牌必须带入商品ID",groups = {ModifyMaker.class})
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotBlank(message = "品牌名称必须填写",groups = {AddMaker.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
	@NotBlank(message = "品牌地址必须填写",groups = {AddMaker.class})
	@URL(message = "必须是一个合法的URL地址",groups = {AddMaker.class,ModifyMaker.class})
	private String logo;
	/**
	 * 介绍
	 */
	@NotBlank(message = "描述不能为空",groups = {AddMaker.class})
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(message = "显示状态为0或者1[0-不显示；1-显示]",groups = {AddMaker.class})
	@ListValue(value = {0,1},message = "显示状态为0或者1[0-不显示；1-显示]",groups = {AddMaker.class,ModifyMaker.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotEmpty(message = "首字母必须是a-z或者A-Z")
	@Pattern(regexp="^[a-zA-Z]$",message = "检索首字母必须是一个字母",groups={AddMaker.class,ModifyMaker.class})
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotNull(message = "排序字段不能为空",groups = {AddMaker.class})
	@Min(value = 0 ,message = "排序字段必须是大于0的整数",groups = {AddMaker.class,ModifyMaker.class})
	private Integer sort;

}
