package com.angle.fastgo.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.AttrGroupEntity;

import java.util.Map;

/**
 * 属性分组
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * description: 根据三级分类ID查询 商品的分组信息
     * version: 1.0
     * date: 2020/11/21 21:22
     * author: zhuwei
     * @param params 查询条件
        {
        page: 1,//当前页码
        limit: 10,//每页记录数
        sidx: 'id',//排序字段
        order: 'asc/desc',//排序方式
        key: '华为'//检索关键字
        }
     * @param catelogId 三级分类ID
     * @return PageUtils
     */
    PageUtils queryPageByCategoryId(Map<String, Object> params, Long catelogId);
}

