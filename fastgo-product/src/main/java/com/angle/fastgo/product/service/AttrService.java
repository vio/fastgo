package com.angle.fastgo.product.service;

import com.angle.fastgo.product.vo.AttrVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.AttrEntity;

import java.util.Map;

/**
 * 商品属性
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * description: 保存商品规格参数
     * 这里的[主体, 基本信息] 就是我们的规格参数分组 详情见attr_group表
     * [主体]中的  入网型号，品牌 产品名称等以及 [基本信息]中的 机身长度的属性就是我们的规格参数
     * 对于的表为attr表中,分组和规格参数的管理关系表 为 attr_attrgourp_relation表中
     *
     * 主体
     *    入网型号 SM-G9730
     *    品牌 三星（SAMSUNG）
     *    产品名称 三星 Galaxy S10 8GB+128GB炭晶黑（SM-G9730）
     *    上市年份 2019年
     *    首销日期 8日
     *    上市月份 3月
     *
     * 基本信息
     *     机身长度（mm） 149.9
     *     机身重量（g） 157
     *     机身材质工艺 阳极氧化
     *     机身宽度（mm） 70.4
     *     机身材质分类  金属边框；玻璃后盖
     *     机身厚度（mm） 7.8
     *     运营商标志或内容 无
     * version: 1.0
     * date: 2020/12/1 23:29
     * author: zhuwei
     * @param attrVO 商品规格参数
     * @return
     */
    void saveAttr(AttrVO attrVO);
}

