package com.angle.fastgo.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * description: 修改品牌信息(若修改的品牌信息中包含了名称，需要关联修改数据)
     * version: 1.0
     * date: 2020/11/22 18:36
     * author: zhuwei
     * @param brand
     * @return void
     */
    void updateByIdWithRelDate(BrandEntity brand);
}

