package com.angle.fastgo.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.CategoryBrandRelationEntity;

import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    /**
     * description: 保存品牌关联的分类
     * 由于前台入参中传入的是 {"brandId":1,"catelogId":2} 我们还需要数据库
     * 中查询出品牌名称和分类名称
     * version: 1.0
     * date: 2020/11/22 18:31
     * author: zhuwei
     * @param categoryBrandRelation {"brandId":1,"catelogId":2}
     * @return void
     */ 
    void saveDetail(CategoryBrandRelationEntity categoryBrandRelation);
}

