package com.angle.fastgo.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:53
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * description: 查询商品的分类以及子分类,并且用树形结构展示
     * version: 1.0
     * date: 2020/11/7 14:32
     * @author: zhuwei
     * @return List<CategoryEntity>
     */
    List<CategoryEntity> listTree() ;

    /**
     * description: 批量删除分类ID(***注意 删除前需要校验该分类是否被其他引用)
     * version: 1.0
     * date: 2020/11/8 16:05
     * @author: zhuwei
     * @param ids 待删除的分类id集合
     * @return void
     */
    void batchDeleteByIds(List<Long> ids);

    /**
     * description: 查询三级分类的路基(比如我传入三级分类ID225 返回该三级分类的路径[2,35,225])
     * version: 1.0
     * date: 2020/11/22 16:45
     * author: zhuwei
     * @param catelogId 三级分类ID
     * @return List<Long> 该三级分类所在的路径
     */
    List<Long> qryCategoryPathById(Long catelogId);

    /**
     * description: 修改商品分类(若分类名称改变了,我们需要修改级联表中的数据)
     * version: 1.0
     * date: 2020/11/22 18:44
     * author: zhuwei
     * @param category
     * @return void
     */
    void updateByIdWithRelDate(CategoryEntity category);
}

