package com.angle.fastgo.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.SpuImagesEntity;

import java.util.Map;

/**
 * spu图片
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:52
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

