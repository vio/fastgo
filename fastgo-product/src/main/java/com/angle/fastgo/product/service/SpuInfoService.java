package com.angle.fastgo.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 15:55:52
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

