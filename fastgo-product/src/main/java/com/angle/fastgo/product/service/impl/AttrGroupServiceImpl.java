package com.angle.fastgo.product.service.impl;

import com.angle.fastgo.product.constant.PMSConst;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.Query;

import com.angle.fastgo.product.dao.AttrGroupDao;
import com.angle.fastgo.product.entity.AttrGroupEntity;
import com.angle.fastgo.product.service.AttrGroupService;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByCategoryId(Map<String, Object> params, Long catelogId) {
        /**
         * 若传入的是顶级分类的,则查询所有的
         */
        if(PMSConst.CATEGORY_TOP_LEVEL == catelogId) {
            return queryPage(params);
        }else {
            QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>().eq("catelog_id",catelogId);
            //查询页面带了关键搜索字
            String searchKey = (String) params.get("key");
            if(!StringUtils.isEmpty(searchKey)) {
                wrapper.and((item)->{
                    item.eq("attr_group_id",searchKey).or().like("attr_group_name",searchKey);
                });
            }
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params),wrapper);
            return new PageUtils(page);
        }
    }

}