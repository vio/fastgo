package com.angle.fastgo.product.service.impl;

import com.angle.fastgo.product.constant.PMSConst;
import com.angle.fastgo.product.entity.CategoryBrandRelationEntity;
import com.angle.fastgo.product.service.CategoryBrandRelationService;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.angle.fastgo.common.utils.PageUtils;
import com.angle.fastgo.common.utils.Query;

import com.angle.fastgo.product.dao.CategoryDao;
import com.angle.fastgo.product.entity.CategoryEntity;
import com.angle.fastgo.product.service.CategoryService;

/**
 * description: 商品分类service
 * version: 1.0
 * date: 2020/11/8 14:15
 * @author: zhuwei
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listTree() {
        //1:查询所有的商品分离
        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);

        //2:过滤查询出所有的一级分类(也就是顶级分类)
        List<CategoryEntity> degreecategoryEntityList =  categoryEntityList.stream().filter(categoryEntity->
            //2.1收集所有的顶级分类的商品
            categoryEntity.getParentCid().equals(PMSConst.CATEGORY_TOP_LEVEL)
        ).map((categoryEntity)->{
            //2.2(顶级分类设置自己的子分类(二级分类))
            categoryEntity.setChildren(fillChildCategory(categoryEntity,categoryEntityList));
            return categoryEntity;
        }).sorted((item1,item2)->{
            //2.3排序
            Integer item1Sort = item1.getSort()==null?0:item1.getSort();
            Integer item2Sort = item2.getSort()==null?0:item2.getSort();
            return item1Sort-item2Sort;
        }).collect(Collectors.toList());

        return degreecategoryEntityList;
    }

    @Override
    public void batchDeleteByIds(List<Long> ids) {
        //TODO 根据后面的业务校验 若分类ID被其他的引用,就不能被删除
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public List<Long> qryCategoryPathById(Long catelogId) {
        List<Long> catelogPath = new ArrayList<>();
        qryCategoryPathWithRecursion(catelogId,catelogPath);
        Collections.reverse(catelogPath);
        return catelogPath;
    }

    @Override
    public void updateByIdWithRelDate(CategoryEntity category) {
        this.updateById(category);
        if(!StringUtils.isEmpty(category.getName())) {
            CategoryBrandRelationEntity categoryBrandRelationEntity = new CategoryBrandRelationEntity();
            categoryBrandRelationEntity.setCatelogName(category.getName());
            categoryBrandRelationEntity.setCatelogId(category.getCatId());
            UpdateWrapper<CategoryBrandRelationEntity> updateWrapper = new UpdateWrapper<CategoryBrandRelationEntity>().eq("catelog_id",category.getCatId());
            categoryBrandRelationService.update(categoryBrandRelationEntity,updateWrapper);
        }
    }

    /**
     * description: 递归查询三级分类的路基
     * version: 1.0
     * date: 2020/11/22 16:52
     * author: zhuwei
     * @param catelogId 三级分类ID
     * @param cateLogPath 用于收集三级分类
     * @return List<Long>
     */
    public List<Long> qryCategoryPathWithRecursion(Long catelogId,List<Long> cateLogPath) {
        cateLogPath.add(catelogId);
        CategoryEntity categoryEntity = baseMapper.selectById(catelogId);
        if(PMSConst.CATEGORY_TOP_LEVEL.equals(categoryEntity.getParentCid())) {
            qryCategoryPathWithRecursion(categoryEntity.getParentCid(),cateLogPath);
        }
        return cateLogPath;
    }

    /**
     * 递归设置子分类
     * @param curCategoryEntity 当前的分类对象
     * @param categoryEntityList  所有的分类对象
     * @return List<CategoryEntity>
     */
    private List<CategoryEntity> fillChildCategory(CategoryEntity curCategoryEntity, List<CategoryEntity> categoryEntityList) {
        List<CategoryEntity> categoryEntities =  categoryEntityList.stream().filter(item->
            //过滤出集中属于curCategoryEntity的子分类
            item.getParentCid().equals(curCategoryEntity.getCatId())
        ).map((categoryEntity -> {
            //(递归设置设置分类 非顶级分类设置自己的子分类)
            categoryEntity.setChildren(fillChildCategory(categoryEntity,categoryEntityList));
            return categoryEntity;
        })).sorted((item1,item2)->{
            //排序
            Integer item1Sort = item1.getSort()==null?0:item1.getSort();
            Integer item2Sort = item2.getSort()==null?0:item2.getSort();
            return item1Sort-item2Sort;
        }).collect(Collectors.toList());
        return categoryEntities;
    }


}