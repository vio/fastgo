package com.angle.fastgo.product.vo;

import com.angle.fastgo.product.entity.AttrEntity;
import lombok.Data;

/**
 * description: 规格参数VO对象
 * version: 1.0
 * date: 2020/12/1 23:26
 * author: zhuwei
 */
@Data
public class AttrVO extends AttrEntity {

    /**
     * 属性分组ID 用于管理属性分组
     */
    private Long attrGroupId;
}
