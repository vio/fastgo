package com.angle.fastgo.product;

import com.angle.fastgo.product.entity.BrandEntity;
import com.angle.fastgo.product.service.BrandService;
import com.angle.fastgo.product.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class FastgoProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void contextLoads() {
    }

    @Test
    public void saveBrandTest() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("苹果12");
        brandEntity.setDescript("最好看的手机");
        brandService.save(brandEntity);
    }

    @Test
    public void findCatelogPathTest() {
        List<Long> catelogPath = categoryService.qryCategoryPathById(225L);
        log.info("catelogpath:{}",catelogPath);
    }

}
