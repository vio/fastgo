package com.angle.fastgothirdparty.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.angle.fastgo.common.utils.DateUtils;
import com.angle.fastgo.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * description: 阿里云图片存储
 * version: 1.0
 * date: 2020/11/20 22:32
 * author: zhuwei
 */
@RequestMapping("thirdparty/oss")
@RestController
@Slf4j
public class OssController implements InitializingBean {
    /**
     * 访问账户
     */
    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKey;

    /**
     * 访问断点
     */
    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endpoint;

    /**
     * 访问的bucket
     */
    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucket;

    @Autowired
    private OSS ossClient;

    private String hostName;

    /**
     * description: 获取OSS签名
     * version: 1.0
     * date: 2020/11/20 23:49
     * author: zhuwei
     * @return R
     */
    @RequestMapping("/generatorOssSign")
    public R generatorOssSign() {
        // 用户上传文件时指定的前缀
        String dateDir = DateUtils.format(new Date(),DateUtils.DATE_PATTERN);
        try {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            // PostObject请求最大可支持的文件大小为5 GB，即CONTENT_LENGTH_RANGE为5*1024*1024*1024。
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dateDir);

            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = ossClient.calculatePostSignature(postPolicy);

            Map<String, Object> respMap = new HashMap<>();
            respMap.put("accessid", accessKey);
            respMap.put("policy", encodedPolicy);
            respMap.put("signature", postSignature);
            respMap.put("dir", dateDir);
            respMap.put("host", hostName);
            respMap.put("expire", String.valueOf(expireEndTime / 1000));
            return R.ok().put("data",respMap);
        } catch (Exception e) {
            log.error("请求oss签名异常:{}",e.getMessage());
            return R.error("请求oss签名异常");
        } finally {
            ossClient.shutdown();
        }
    }

    /**
     * description: 初始化上传地址
     * version: 1.0
     * date: 2020/11/20 22:43
     * author: zhuwei
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        StringBuffer hostName = new StringBuffer("https://");
        hostName.append(bucket).append(".").append(endpoint);
        log.info("上传OSS地址:{}",hostName);
        this.hostName = hostName.toString();
    }
}
