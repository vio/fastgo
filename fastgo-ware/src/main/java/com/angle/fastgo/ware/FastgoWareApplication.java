package com.angle.fastgo.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastgoWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastgoWareApplication.class, args);
    }

}
