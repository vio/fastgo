package com.angle.fastgo.ware.dao;

import com.angle.fastgo.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:09:08
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
