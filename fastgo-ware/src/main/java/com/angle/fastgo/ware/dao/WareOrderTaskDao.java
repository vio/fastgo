package com.angle.fastgo.ware.dao;

import com.angle.fastgo.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:09:08
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
