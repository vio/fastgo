package com.angle.fastgo.ware.dao;

import com.angle.fastgo.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author smlz
 * @email smlz@qq.com
 * @date 2020-10-25 16:09:08
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
